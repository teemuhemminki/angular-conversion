# AngularConversion

This is an assignment from JAMK course of Web Application Development. Commenting of code has been done in Finnish.  

The task was to convert small application from old AngularJS to modern Angular.  

Assignment also asked what are similarities and differences between AngularJS and Angular2+.  

I find that AngularJS and Angular2+ are similar mostly with how they bind data and some other parts of the syntax and that both do use routing. Otherwise Angular has vastly changed.  

The core design has gone from using controllers and factories into component oriented design. Angular2+ implements components which are independent parts that can be edited independently without affecting other components. Components declare their own template, instead of router. $scope is also gone in Angular2+, they are instead replaced by adding reference in constructor of component.  

Angular2+ depends on typescript and is more strict framework. TypeScript also offers powerful tools like classes and lambdas.

During this conversion I observed that html needed only small alterations, while script needed to be completely overwritten.  

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.3. Remember to run `npm install` after cloning this repository.  

This project also uses ng2-bootstrap. Install it with `npm install ngx-bootstrap --save`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
