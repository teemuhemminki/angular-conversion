import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LocalStorageModule } from 'angular-2-local-storage';

import { AppComponent } from './app.component';
import { FormComponent } from './form.component';
import { ShowComponent } from './show.component';
import { RegistrationService } from './registration.service';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    ShowComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    LocalStorageModule.withConfig({
      prefix: 'ilmoittautuminen',
      storageType: 'localStorage'
    }),
    RouterModule.forRoot([
      {
        path: 'form',
        component: FormComponent
      },
      {
        path: 'show',
        component: ShowComponent
      }
    ])
  ],
  providers: [ RegistrationService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
