// form komponentti käsittelee ilmoittautumiskaavakkeeseen liittyvät toiminnot

import { Component, ViewChild } from '@angular/core';

import { Router } from '@angular/router';

import { RegistrationService } from './registration.service';
import { Registration } from './registration';

@Component({
  selector: 'app-form-component',
  templateUrl: './form.component.html'
})

export class FormComponent {

  formdata = new Registration(0, '', '', '', ''); // Luodaan tyhjä ilmoittautuminen
  @ViewChild('emailField') email; // Antaa tarkastella html templaatin email sisältöä

  constructor(
    private router: Router,
    private registrationService: RegistrationService
  ) { }

  // Lisää datan rekisteröityihin
  addData() {
    if (this.email.valid) { // Tarkistaa onko sähköpostiosoite validi
      // Muokataan sauna tieto arvon mukaan jooksi tai eiksi
      if (this.formdata.sauna) {
        this.formdata.sauna = 'joo';
      } else {
        this.formdata.sauna = 'ei';
      }
      // Käytetään rekisteröinti palvelua rekisteröinnin tallentamiseksi
      this.registrationService.saveRegistration(this.formdata);

      // Jos tiedot lähetetään, vaihdetaan takaisin ilmoittautuneet sivulle
      this.router.navigate(['/show']);
    }
  }
}
