// Show component hallinnoi ilmoittautumisien näyttämistä käyttäjälle

import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

import { RegistrationService } from './registration.service';
import { Registration } from './registration';

@Component({
  selector: 'app-show-component',
  templateUrl: './show.component.html'
})
export class ShowComponent implements OnInit {

  private ilmot = Array<Registration>(); // Ilmoittautumis taulukko jota html näyttää

  constructor (
    private registrationService: RegistrationService
  ) { }

  // Initialisoidessa haetaan rekisteröitymiset
  ngOnInit() {
    this.getRegistrations();
  }

  // Hakee ilmoittautumiset registrationServiceltä
  getRegistrations() {
    this.ilmot = this.registrationService.getRegistrations();
  }

}
