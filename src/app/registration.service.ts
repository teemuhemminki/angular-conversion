// registration.service hakee ja tallettaa datan
import { Injectable } from '@angular/core';

// Alla erikseen asennetun local storage service node moduulin importointi
import { LocalStorageService } from 'angular-2-local-storage';

import { Registration } from './registration';

@Injectable()

// HUOM: koska käytetään localstoragea, on toteutus melko kömpelö enkä keskittynyt sen optimointiin
export class RegistrationService {

  constructor (
    private localStorageService: LocalStorageService
  ) { }

  // Palauttaa joko localstoragen tiedot tai tyhjän taulukon
  getRegistrations(): Array<Registration> {
    if (this.localStorageService.get('ilmot') != null) {
      return this.localStorageService.get('ilmot') as Array<Registration>;
    } else {
      return [];
    }
  }

  // Tallettaa localstorageen uuden rekisteröidyn taulukon.
  saveRegistration(reg) {
    const newRegs = this.getRegistrations();
    const nReg = reg;
    nReg.id = newRegs.length + 1; // Tosielämässä tietokannan automaattinen id määritys hoitaisi asian
    newRegs.push(reg);
    this.localStorageService.set('ilmot', newRegs);
  }

}
