// Ilmoittautumis olio jota käytetään ilmoittautumisien hallitsemiseen


export class Registration {

  id: number;
  name: string;
  email: string;
  food: string;
  sauna: string;

  constructor(
    id: number,
    name: string,
    email: string,
    food: string,
    sauna: string
  ) {}
}
